<?php

Namespace App\Repositories\Task;

interface TaskInterface
{
    /*
     *
     *
     *
     */
    public function getAll();

    /*
     *
     *
     *
     */
    public function find($id);

    /*
     *
     *
     *
     */
    public function delete($id);

}

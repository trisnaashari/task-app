<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $fillable = ['name', 'description', 'due_date'];

    protected $dates = [
        //'due_date'
    ];

    /*
     * Custom formar for due_date
     *
     * @param $value
     * @return string
     */
    /*
    public function getDueDateAttribute($value)
    {
        return \Carbon\Carbon::parse($this->attributes['due_date'])
                    ->format('M d, Y');
    }
     */

    /*
     * Custom format for created_at
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
                   ->format('M d, Y H:i');
    }

    /*
     * Custom format for updated_at
     *
     * @param $value
     * @return string 
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    }

    /*
     * Make sure that name is capitalized first character of each word
     *
     * @param $value
     * @return string
     */
    public function setNameAttribute($value)
    {
        return $this->attributes['name'] = ucwords($value);
    }

    /*
     * Make sure that description is capitalized first character
     *
     * @param value
     * @return string
     */
    public function setDescriptionAttribute($value)
    {
        return $this->attributes['description'] = ucfirst($value);
    }

}

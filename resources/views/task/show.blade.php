@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            View Task
        </div>
        <div class="panel-body">
            <div class="pull-right">
                <a class="btn btn-default" href="{{ route('task.index') }}">Go Back</a>
            </div>
            <div class="form-group">
                <strong>Name: </strong> {{ $task->name }}
            </div>
            <div class="form-group">
                <strong>Description: </strong> {{ $task->description }}
            </div>
            <div class="form-group">
                <strong>Due Date: </strong> {{ $task->due_date }}
            </div>
            <div class="form-group">
                <strong>Created At: </strong> {{ $task->created_at }}
            </div>
            <div class="form-group">
                <strong>Updated At: </strong> {{ $task->updated_at }}
            </div>
        </div>
    </div>

@endsection

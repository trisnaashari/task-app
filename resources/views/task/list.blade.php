@extends('layouts.app')

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                Task List
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="pull-right">
                        <a href="{{ route('task.create') }}" class="btn btn-default">Add New Task</a>
                    </div>
                </div>

            <table class="table table-bordered table-stripped">
                <tr>
                    <th width="20">No</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Due Date</th>
                    <th width="300">Action</th>
                </tr>
                @if ($tasks->count())
                    @foreach ($tasks as $key => $task)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $task->name }}</td>
                            <td>{{ $task->description }}</td>
                            <td>{{ $task->due_date }}</td>
                            <td>
                                <a class="btn btn-success" href="{{ route('task.show',$task->id) }}">Show</a>
                                <a class="btn btn-primary" href="{{ route('task.edit',$task->id) }}">Edit</a>
                                {{ Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['task.destroy', $task->id],
                                    'style'=>'display:inline'])
                                }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">Tasks not found!</td>
                    </tr>
                @endif
            </table>

            {{ $tasks->render() }}

        </div>
    </div>

@endsection

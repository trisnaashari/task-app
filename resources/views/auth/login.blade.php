@include('layouts.headers.header')

    <div id="wrapper">
        <div class="auth-slider-container text-left">
            <div id="auth-intro">
                <div class="section" style="position: relative;">
                    <div class="hidden-xs hidden-sm" 
                        style="position: absolute; bottom: 100px; left: 0; right: 0; margin: 0 auto; color: #fff; z-index: 3000; text-align: center;">
                        {{ trans('auth.no_account') }}
                        &nbsp;
                        <a href="{{ url('auth/register') }}" class="load-ajax-content" style="color: #fff">
                            <u>{{ trans('auth.register_now') }}</u>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="auth-form-container text-center">
            <div class="auth-helper">
                <div class="auth-box">
                    <div class="auth-overlay-bg"></div>
                        <h1 class="fs-xs-24 fs-sm-32 fs-md-42 fs-lg-42 fw600">{{ config('settings.app.name') }}</h1>
                        <h2 class="fs14 fw100">{{ trans('auth.login_to_continue') }}</strong></h2>
                        <form id="form" class="m-t" role="form"
                            action="{{ url('login/authentication') }}" 
                            style="margin-top: 50px;">
                                <div class="form-group full-width has-feedback">
                                    <div class="col-xs-12 input-group no-padding">
                                        <div class="input-group-addon hidden-sm hidden-md hidden-lg">
                                            <i class="material-icons fs24">account_circle</i>
                                        </div>
                                        <input type="email" 
                                            name="email" 
                                            id="input-auth-identifier" 
                                            class="form-control input-lg" 
                                            placeholder="{{ trans('auth.email') }}" 
                                            data-error="{{ trans('auth.email').trans('msg.is_required') }}"
                                            autocomplete="off"
                                            required>
                                            <span class="bar"></span>
                                            <span class="help-block with-errors pull-right"></span>
                                    </div>
                               </div>
                               <div class="form-group full-width has-feedback">
                                    <div class="col-xs-12 input-group no-padding">
                                        <div class="input-group-addon hidden-sm hidden-md hidden-lg">
                                             <i class="material-icons fs24">lock</i>
                                        </div>
                                        <input type="password" 
                                            name="password" 
                                            id="input-auth-password" 
                                            class="form-control input-lg" 
                                            placeholder="{{ trans('auth.password') }}"
                                            data-error="{{ trans('auth.password') }}.{{ trans('msg.is_required') }}"
                                            data-minlength="6"
                                            autocomplete="off"
                                            required>
                                        <span class="bar"></span>
                                        <span class="help-block with-errors pull-right"></span>
                                    </div>
                               </div>
                               <button type="submit" 
                                    id="btn-auth-login" 
                                    class="btn btn-primary 
                                    btn-lg block full-width mt15" 
                                    data-loading-text="{{ trans('msg.processing') }}">{{ trans('auth.login') }}</button>
                               <div class="auth-feedback mt15"></div>
                          </form>
                     </div>
                </div>
           </div>
      </div>
    </div>

    <script>
      $(document).ready(function () {
           $('#form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                     var FORM = $(this);
                    $.ajax({
                       url: FORM.attr('action'),
                       type: "POST",
                       data: FORM.serialize(),
                       dataType: "json",
                       cache: false,
                       beforeSend: function () {
                            $('#btn-auth-login').bootstrapButton('loading');
                            $('.auth-feedback').html('<div class="alert alert-default flat no-border no-margin"><h3 class="fs24 fw600">{{ trans('msg.please_wait') }}</h3><h4 class="fs14 fw400 lh20">{{ trans('msg.processing_your_request') }}</h4></div>');
                       },
                       success: function (response) {
                            if (response.status === true) {
                                 $('#btn-auth-login').html('<div class="spinner-white sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>');
                                 $('.auth-feedback').html(response.msg);
                                 var href = response.url;
                                 if (href.indexOf(document.domain) > -1 || href.indexOf(':') === -1) {
                                      history.pushState({}, '', href);
                                      loadContent(href);
                                      return false;
                                 }
                            } else {
                                 $('#btn-auth-login').bootstrapButton('reset');
                                if (response.type === 'WRONG_IDENTIFIER' || response.type === 'WRONG_PASSWORD' || response.type === 'BANNED' || response.type === 'INACTIVE')
                                    $('.auth-feedback').html('<div class="alert alert-danger flat no-border no-margin"><h3 class="fs24 fw600">' + response.msg.title + '</h3><h4 class="fs14 fw400 lh20">' + response.msg.desc + '</h4></div>');
                                if (response.type === 'MAX_ATTEMPTS')
                                    $('.auth-feedback').html('<div class="alert alert-danger flat no-border no-margin"><h3 class="fs24 fw600">' + response.msg.title + '</h3><h4 class="fs14 fw400 lh20">' + response.msg.desc + '</h4></div>');
                            }
                       },
                       error: function (jqXHR, textStatus, errorThrown) {
                            $('#btn-auth-login').bootstrapButton('reset');
                            $('.auth-feedback').empty();
                            ajaxErrorHandler(jqXHR, jqXHR.responseText, textStatus);
                       }
                    });
                }
                return false;
           })
      });
    </script>

@include('layouts.footers.footer')



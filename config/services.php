<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
	'api_key' => env('MAILGUN_API_KEY'),
	'smtp_host' => env('MAILGUN_SMTP_HOST'),
	'smtp_port' => env('MAILGUN_SMTP_PORT'),
	'smtp_username' => env('MAILGUN_SMTP_USERNAME'),
	'smtp_password' => env('MAILGUN_SMTP_PASSWORD'),
	'smtp_secure' => env('MAILGUN_SMTP_SECURE'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
	'client_id' => env('GOOGLE_CLIENT_ID'),
	'client_secret' => env('COOGLE_CLIENT_KEY'),
	'api_key' => env('GOOGLE_API_KEY'),
    ]

];
